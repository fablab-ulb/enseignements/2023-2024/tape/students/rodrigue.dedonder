# Journal de bord d'Arsène Lupin

## Qui suis-je ?

Je m'appelle Rodrigue De Donder, je suis en deuxième année de master d'ingénieur physicien à l'ULB, dans le cadre du cours "Advanced techniques of experimental physics" j'ai du travailler sur le projet UC2 et je détaille mon travail dans ce document. 

![image d'un microscope UC2](img/UC2-image.png)

## Introduction 

Le projet UC2 a pour objectif de créer un microscope peu cher et facile à installer, calibrer et modifier si nécessaire. Ici, le microscope est un assemblage de cube, qui sont détachables les uns des autres, chaque cube est soit vide et sert au "squelette" du microscope, soit il contient un composant et a donc un rôle supplémentaire. 
Tous les cubes ayant les mêmes dimensions, la première contrainte est donc d'avoir des composants qui arrive à fonctionner à une distance fixe les uns des autres.(dans la réalité, les distances sont légèrement adaptables).
La deuxième contrainte est que cette facilité de modifications et d'interchangabilité des cubes font que le microscope devient très rapidement volumineux et surtout peu stable. En effet, des joints sont placés entre chaque cube, ces joints contiennent des aimants qui permettent aux cubes d'être accrochés entre eux et d'être bien alignés, le problème étant que les aimants ne sont pas très puissant et que donc le microscope est au global fragile et instable.

## Septembre 2023

Tout d'abord, j'ai dû me familiariser avec le microscope et la microscopie en général, une approche simple est de commencer avec un système de plusieurs lentilles alignées et donc on peut changer la distance entre chacune d'elles, et de décrire ce système par l'approche matricielle. Le système le plus basique étant de prendre deux lentilles, c'est celui-ci qui sera détaillé ci-dessous.

### Formalisme matriciel

Dans ce formalisme, on considère que la lumière se propagent comme un faisceau non diffracté, on considère également que le faisceau reste dans le plan yz, donc sa composante en x reste nulle tout du long, il se propage dans la direction z et donc on va pouvoir exprimé sa hauteur $y$ et son angle d'incidence $\theta$ en fonction de z.

#### Matrice de propagation 

De manière assez immédiate, on peut voir qu'après une avancée dans la direction z, les nouvelles valeurs, $y'$ et $\theta'$, se calculent par:
$$
y'=y+tan(\theta)*z\\
tan(\theta')=tan(\theta)\\
$$
On peut donc écrire ce système sous la forme suivante:
$$
\left(\begin{array}{cc} 
y' \\
tan(\theta')
\end{array}\right)=
\left(\begin{array}{cc} 
1 & z\\
0 & 1
\end{array}\right)
\left(\begin{array}{cc} 
y \\
tan(\theta)
\end{array}\right)
$$

<div align="center">
<img src="img/matrice1.png" width="35%" height="35%">
<figcaption>Propagation d'un faisceau dans le vide</figcaption>
<div align="left">

#### Matrice d'une lentille

Pour cette matrice-ci, considérons un ensemble de faisceaux parallèles, on sait par les propriétés d'une lentille qu'ils doivent tous converger en un point à la distance focale $f$ de la lentille, donc au passage d'une lentille, la hauteur ne change pas, mais l'angle est modifié. En considérant que le centre de la lentille est en $y=0$, la condition mentionnée précédemment devient: 
$$
y'+tan(\theta')*f=0
$$
Le système devient donc: 
$$
y'=y\\
tan(\theta')=-y'/f
$$
Et dans la forme matricielle:
$$
\left(\begin{array}{cc} 
y' \\
tan(\theta')
\end{array}\right)=
\left(\begin{array}{cc} 
1 & 0\\
-1/f & 1
\end{array}\right)
\left(\begin{array}{cc} 
y \\
tan(\theta)
\end{array}\right)
$$

<div align="center">
<img src="img/matrice2.png" width="30%" height="30%">
<figcaption>Transformation du faisceau au passage d'une lentille</figcaption>
<div align="left">

A noter que cette matrice décrit juste la transformation du faiseau qui traverse la lentille, et comme on suppose la lentille suffisamment fine, cette matrice ne décrit aucune propagation, ce qui explique pourquoi la hauteur ne peut pas avoir changée.

#### Matrice de propagation couplée à une lentille

Une étape intermédiaire, qui va permettre de rendre les calculs plus lisibles, est de combiner une propagation avant et après une lentille, en traversant d'abord une distance $d_1$, puis en traversant une lentille, enfin une distance $d_2$, la matrice totale va prendre la forme suivante:
$$
\left(\begin{array}{cc} 
1 & d_{2}\\
0 & 1
\end{array}\right)
\left(\begin{array}{cc} 
1 & 0\\ 
-1/f & 1
\end{array}\right)
\left(\begin{array}{cc} 
1 & d_{1}\\
0 & 1
\end{array}\right)
=
\left(\begin{array}{cc} 
1-d_{2}/f & d_{1}+d_{2}-d_1d_2/f\\ 
-1/f & 1-d_{1}/f
\end{array}\right)
$$

#### Matrice du système à deux lentilles 

Les deux matrices trouvées précédemment suffisent à décrire le système suivant:
Un objet situé à une distance $d_1$ de la première lentille, de distance focale $f_1$, la deuxième lentille, de distance focale $f_2$ suit la première d'une distance $d_2+d_3$, enfin l'observateur suit la deuxième lentille d'une distance $d_4$. La matrice totale du système va prendre la forme suivante:
$$
\left(\begin{array}{cc} 
1-d_{4}/f_{2} & d_{3}+d_{4}-d_3d_4/f_{2}\\ 
-1/f_{2} & 1-d_{3}/f_{2}
\end{array}\right)
\left(\begin{array}{cc} 
1-d_{2}/f_{1} & d_{1}+d_{2}-d_1d_2/f_{1}\\ 
-1/f_{1} & 1-d_{1}/f_{1}
\end{array}\right)
$$
La matrice total est le produit de ces deux matrices, mais pour ne pas avoir une forme finale trop lourde, on va la garder pour l'instant sous cette forme là.

### Système afocal

En reprenant la matrice ci-dessus, et en considérant $d_{2}=f_{1}$ et $d_{3}=f_{2}$, c'est à dire que la distance séparant les deux lentilles est égale à la somme des distances focales, on se retrouve dans un système afocal. La matrice totale du système devient:

$$
\left(\begin{array}{cc} 
1-d_{4}/f_{2} & f_{2}\\ 
-1/f_{2} & 0
\end{array}\right)
\left(\begin{array}{cc} 
0 & f_{1}\\ 
-1/f_{1} & 1-d_{1}/f_{1}
\end{array}\right)
=
\left(\begin{array}{cc} 
-f_{2}/f_{1} & f_{1}(1-d_{4}/f_{2})+f_{2}(1-d_{1}/f_{1})\\ 
0 & -f_{1}/f_{2}
\end{array}\right)
$$

De la forme de cette matrice, on peut en déduire une propriété:

- La nouvelle inclinaison ne dépend pas de la position initiale du faisceau, donc un ensemble de faisceaux parallèles à l'entrée donnera un ensemble de faisceaux parallèles à la sortie. Leur angle de propagation sera modifié par un même facteur. Les faisceaux seront également rapprochés ou éloignés les uns des autres.

Une deuxième propriété, qui va être démontrée ci-dessous, est que l'on peut éloigner l'objet observé, et toujours le voir avec le même facteur de grossissement. 
Cette seconde propriété permet de connaitre la distance focale d'une lentille. En alignant la lentille avec une autre lentille dont on connait déjà la distance focale, on peut déduire sa distance focale en la bougeant progressivement, lorsqu'on voit que le grossissement ne dépend plus de l'éloignement de l'objet, on est en système afocal et on en déduit donc facilement la distance focale de la lentille.

#### Démonstration

Un système est dit système d'imagerie si tous les faisceaux émis (ou du moins projetés) par l'objet, sont reconcentrés en un point à la sortie. Ceci permet que les faisceaux émis par un point ne créent pas d'abérations en arrivant à la sortie à la position supposée d'un autre point.

Exiger cette condition implique que l'élément en haut à droite de la matrice doit être nul, puisqu'on veut que le $y'$ de sorti soit le même pour tous les faisceaux émis, si on considère l'objet comme ponctuel, tous les faisceaux ont déjà le même $y$ à l'entrée, leur seule différence est donc leur angle de propagation.

Dans le système afocal, la condition prend la forme:
$$
f_{1}(1-d_{4}/f_{2})+f_{2}(1-d_{1}/f_{1})=0
$$
On peut donc exprimer $d_{4}$ en fonction de $d_{1}$:
$$
d_{4}=f_{2}+\frac{f_{2}^{2}}{f_{1}}-\frac{f^{2}_{2}d_{1}}{f^{2}_{1}}
$$

Maintenant on sait à quelle distance doit se placer l'observateur de la deuxième lentille en connaissant la distance qui sépare l'objet de la première. A noter que si l'objet est suffisamment loin, on peut considérer que les rayons émis sont parallèles et on peut se servir de la première propriété.

Maintenant qu'on a annulé la dépendance de l'angle pour la position $y'$ à la sortie, on voit que $y'=\frac{-f_{2}}{f_{1}}y$, on a donc bien un grossissement indépendant des distances.

## Octobre 2023

Maintenant que la manipulation des constituants a été faite et que les outils mathématiques sont présents pour pouvoir décrire n'importe quel système, passons aux deux idées principales qui vont être développées plus bas:

- Le microscope smartphone : Le smartphone reste encore le meilleur moyen de sauvegarder une image, il est donc intéressant d'être capable de poser le smartphone sur l'oculaire pour qu'il puisse photographier ce qu'on peut voir à travers celui-ci. Néanmoins, la caméra d'un smartphone est composée d'une lentille, il faut donc prendre en compte cette nouvelle lentille afin d'être capable d'en déduire le grossissement total. En plus de ça, il faut créer un agencement stable pour pouvoir poser le smartphone sur l'oculaire sans que rien ne s'effondre.

- Le microscope à champ noir : il faut rajouter un stop optique (qui est simplement un cache) qui permettra d'observer les échantillons avec un autre "éclairage", et donc potentiellement voir autrement les échantillons et avec plus de détailles.

Dans ce travail, c'est d'abord le microscope à champ noir qui est présenté, mais les recherches et manipulations des deux sujets ont été faites en parallèles

### Microscope smartphone

#### Prise en compte d'une nouvelle lentille

##### Facteur de grossissement
Lors de l'ajout d'une nouvelle lentille, le facteur de grossissement total est modifié, ce facteur est le rapport entre la taille de l'objet tel qu'on le voit à la sortie et la taille réelle de l'objet.
$$
G=\frac{x_{vu}}{x_{réel}}
$$
Ici, le fait de coupler le smartphone avec le microscope donne l'équation suivante:
$$
G_{tot}=G_{phone}G_{micro}
$$
Si on connait donc les facteurs de grossissement individuels, on sait comment le système total se comporte.

On peut également prendre le problème à l'envers, on mesure le grossissement total et en supposant qu'on a au préalable mesuré le grossissement du smartphone ou du microscope, on peut déduire le grossissement de l'un ou de l'autre. 

##### Distance focale 
On a de manière analogue, avec les distances focales, une équation simple qui permet de connaitre celle du système totale:
$$
\frac{1}{f_{tot}}=\frac{1}{f_{phone}}+\frac{1}{f_{micro}}-\frac{e}{f_{phone}f_{micro}}
$$
Où $e$ est la distance séparant le smartphone et le microscope

Il est facile de connaitre la distance focal du smartphone, soit on connait le modèle et on regarde sur internet les données du fabricant, soit on peut mener la manipulation suivante:

-Prendre une photo d'un même objet fixe à plusieurs distances
-Mesurer manuellement la taille réelle de l'objet, et en déduire le facteur de grossissement. 
-Porter en graphique l'équation suivante: $1/G_{phone}=1+d/f_{phone}$, en mettant en ordonnée l'inverse du grossissement et en abscisse $d$, la distance entre le smartphone et l'objet lors de la photo. La pente de la droite donnera directement l'inverse de la distance focale.

#### Agencement du système

Pour voir à travers l'oculaire, le moyen le plus simple est d'empiler tous les éléments et de regarder par dessus, si l'on veut déposer notre smartphone par dessus pour prendre une photo, il faut créer une tour en parallèle pour que les deux tours combinées soutiennent le smartphone, le tout n'étant vraiment pas stable, il est préférable de réduire la hauteur total du microscope et de rapprocher l'oculaire du sol pour ne pas devoir mettre le smartphone trop en hauteur.

Un premier agencement est fait à l'aide de deux miroirs réfléchissants qui suivent le système, ces miroirs rajoutent du chemin optique et pourraient être responsables d'aberrations, il est donc préférable de ne pas en utiliser trop. Ce premier agencement a la même hauteur maximale que la tour faite initialement, mais il a l'avantage de placer l'oculaire (et donc le smartphone) bien plus bas.

<div align="center">
<img src="img/agence2.png" width="25%" height="25%">
<figcaption>Premier agencement</figcaption>
<div align="left">

Un second agencement a été réalisé, il ne garde qu'un seul miroir et l'essentiel du microscope est au sol, il ne risque donc pas d'y avoir d'effondrement.

<div align="center">
<img src="img/agence1.png" width="40%" height="40%">
<figcaption>Second agencement</figcaption>
<div align="left">

Le second agencement est pratique puisqu'il ne risque pas de causer de casse du microscope à cause d'un effondrement, cependant tout le système s'étale sur la longueur, ce qui n'est pas toujours pratique pour la gestion de l'espace.
Il est important de noter que dans les deux cas, le schéma présenté ne contient pas le "soutien" du smartphone, il suffit, comme expliqué précédemment, de rajouter une tour à côté de l'oculaire pour déposer le smartphone en équilibre sur le soutien et l'oculaire.

##### Matrice d'un miroir

Si l'on souhaite continuer d'utiliser le formalisme matriciel, il faut tenir compte des miroirs réfléchissants. Comme la lentille, le miroir ne change que l'angle de propagation du faisceau, et la variation d'angle ne dépend que de l'angle incident.

$$
\left(\begin{array}{cc} 
1 & 0\\ 
0 & Inv()
\end{array}\right)
$$
Où $Inv()$ est l'opérateur inverse qui renvoie l'inverse de l'argument, en clair le miroir donne en sortie $tan(\theta')=1/tan(\theta)$

Ce calcul est fait dans le cas précis où le miroir est incliné de $45°$, ce qui est le cas dans notre microscope.

A noter que vu le formalisme matriciel qu'on a défini, il n'est pas possible de traiter des rayons qui arrivent parallèles au sol sur le miroir, ils seraient réfléchis verticalement et donneraient des $y$ infinis après une propagation.

###### Démonstration

<div align="center">
<img src="img/miroir2.png" width="30%" height="30%">
<figcaption>Schéma de l'effet d'un miroir sur un faisceau</figcaption>
<div align="left">

On peut voir que les angles $a,b,c$ et $d$ sont les angles d'un quadrilatère, on a donc:
$$
a+b+c+d=360°
$$
On voit rapidement que cette équation est équivalente à: 
$$
b=90°+e-a
$$
L'angle de sortie étant $\theta'=e+b+90°$, on a : 
$$
\theta'=180°-\theta+2e
$$
Dans le cas où $e=45°$, on retrouve bien $tan(\theta')=1/tan(\theta)$

### Microscope à champ noir

Sans rentrer dans les détails, une lentille permet d'avoir la transformée de Fourier du signal dans le plan focal, on peut donc travailler dans le spectre spatial (qui est encore différent du spectre fréquentiel) en rajoutant des éléments au système dans la plan focal de la lentille. Une des propriétés de la transformée de Fourier est que lorsqu'on l'applique deux fois sur un objet (ici l'ensemble des faisceaux de lumière), on revient sur l'objet initial qui simplement subit une homothétie, donc rajouter une deuxième lentille permet de revenir dans le domaine spatial initial. 

Cette explication permet de comprendre l'idée générale derrière un microscope à champ noir. Comme le montre le schéma ci dessous, on va ne garder que les faisceaux dont la composante en x et en y du vecteur d'onde, respectivement $k_{x}$ et $k_{y}$, sont supérieures à une valeur déterminée par les dimensions du système. Lorsque qu'un faisceau de lumière ne rencontre aucun spécimen, il ne sera pas diffracté et va donc suivre un trajet qui va le mener à un stop optique, qui est simplement un cache qui va empêcher sa progression. Lorsque le faisceau va rencontrer un spécimen, il sera diffracté et va prendre un autre chemin qui va lui permettre de contourner le stop optique. Ce sont donc bien les objets qui vont apparaitre comme lumineux, et le fond comme noir.

<div align="center">
<img src="img/champnoir2.png" width="50%" height="50%">
<figcaption>Première configuration pour un microscope à champ noir, la couleur des faisceaux qui n'atteignent pas la deuxième lentille a été modifiée pour une meilleure compréhension</figcaption>
<div align="left">

Ceci est un premier moyen de créer un microscope à champ sombre, une autre possibilité est reprise dans le schéma qui suit:

<div align="center">
<img src="img/champnoir1.png" width="50%" height="50%">
<figcaption>Deuxième configuration pour un microscope à champ noir</figcaption>
<div align="left">

La différence ici est que les faisceaux du fond ne sont pas arrêtés, mais ne sont juste pas captés par la lentille à cause de leur angle de propagation trop grand.
Par la suite, on va privilégier cette deuxième manière et donc établir ses conditions de fonctionnement qui lui sont spécifiques.

Le cache est disponible sous la forme d'un code OpenSCAD dans le dossier du même nom, il est constitué de deux parties:

- Le support avec un trou au centre

- Le disque intérieur possédant 3 tiges pour se fixer dans le support

Le processus du microscope à champ noir a deux avantages: 

- Il permet de mieux résoudre l'échantillon, car la lumière est diffusée selon la matière qu'elle rencontre, ceci permet d'avoir un autre aperçu de la composition et de la géométrie du spécimen étudié.

- Un effet de halo apparait autour du spécimen, ce qui permet de mieux le distinguer du fond, ce halo vient du fait que le spécimen est un objet haute fréquence, alors que la fond est basse fréquence, la séparation nette entre les deux produit donc ce halo. La présence de ce halo permet de confirmer que le microscope est bien en champ noir, c'est une bonne première indication quant à l'efficacité d'un prototype.

#### Conditions de fonctionnement 

Afin que ce système fonctionne au mieux, certaines conditions sont à prendre en compte. Si elles ne sont pas respectées, le microscope fonctionne encore mais la qualité du champ noir se réduit.

- Les faisceaux à l'entrée doivent être le plus parallèles possible à l'axe de propagation $z$, des faisceaux qui ne sont pas parallèles peuvent être captés par l'objectif sans avoir rencontré de spécimen. Pour avoir les faisceaux les plus parallèles possibles, il faut que le cercle de lumière soit le plus fin possible, c'est à dire que le disque intérieur ait un rayon très proche du disque extérieur. Il faut également que le cache soit épais pour bloquer au maximum les faisceaux qui ne passent pas exactement à travers le cercle.
Une autre manière d'avoir des rayons parallèles est de placer la source de lumière et le cache suffisamment loin de la lentille, mais alors le microscope peut prendre des dimensions trop importantes.

- La lame de verre contenant les spécimens doit être exactement dans le plan focal de la première lentille. Ceci se justifie par le fait que c'est dans le plan focal qu'on a la transformée de Fourier, et qu'on peut sélectionner correctement les vecteurs d'onde qu'on veut.

- Le système doit être idéalement plongé dans le noir, si la pièce est éclairée et que le système est à l'air libre, cet éclairage constitue un bruit de fond lors de la mesure.

- La lentille de l'objectif ne doit pas trop grande, sinon elle pourrait quand même capter les faisceaux non désirés.

Cette dernière propriété se retranscrit par l'inégalité suivante: 
$$
d > \frac{r_2}{r_1}f
$$

$d$ est la distance séparant l'objectif de la lame de verre, $f$ la distance focale de la première lentille, $r_{1}$ le rayon de la première lentille et $r_{2}$ le rayon de la lentille de l'objectif.

##### Démonstration

Considérons le cas limite, où les rayons de plus grand angle, donc les rayons sur le bord de la première lentilles, sont quand même captés par l'objectif, entre la sortie de la première lentille, et l'entrée de la lentille de l'objectif, il y a une conservation de l'angle de propagation, on peut donc déduire d'abord que pour se concentrer en un point dans le plan focal, l'angle de propagation maximal doit suivre la loi suivante:
$$
tan(\theta)=f/r_{1}
$$
Ensuite, pour revenir sur les bords de l'objectif, l'angle doit vérifier la relation suivante:
$$
tan(\theta)=d/r_{2}
$$
On retombe sur l'égalité $d = \frac{r_2}{r_1}f$, ceci étant le cas limite, pour ne pas avoir les rayons des bords, on la transforme en inégalité.

#### Résultats

Regardons d'abord la différence de la vue de grains de sable avec un microscope à champ clair, qui est la version "classique" du microscope, et un microscope à champ noir. 

<div align="center">
<img src="img/SableChampClair.jpg" width="20%" height="20%">
<figcaption>Aperçu de grains de sable dans un microscope à champ clair</figcaption>
<div align="left">

<div align="center">
<img src="img/SableChampNoir.jpg" width="20%" height="20%">
<figcaption>Aperçu de grains de sable dans un microscope à champ noir</figcaption>
<div align="left">

On voit comme prévu plus de détailles sur le microscope à champ noir, les gouttes d'eau condensées sont devenues visibles et on peut mieux résoudre les grains de sable.

Regardons maintenant l'effet d'un microscope à champ noir sur des billes de verre de 200µm de diamètre, ces objets étant moins "hasardeux", ils sont plus faciles à analyser, en l'occurence, on voit mieux sur les billes l'effet de halo que sur les grains de sable.

<div align="center">
<img src="img/BillesChampNoir.jpg" width="20%" height="20%">
<figcaption>Aperçu de billes de verre dans un microscope à champ noir</figcaption>
<div align="left">

<div align="center">
<img src="img/BillesChampNoir2.jpg" width="20%" height="20%">
<figcaption>Aperçu de billes de verre dans un microscope à champ noir (avec éclairage extérieur)</figcaption>
<div align="left">

La deuxième image présentent les mêmes billes, à la différence qu'ici la pièce était allumée lors de la prise de la photo, on retrouve bien un bruit de mesure qui, ici, vient principalement changer la couleur des billes. Cette deuxième image prouve l'importance de plonger le système dans le noir.

## Pistes d'amélioration

Ce travail s'est principalement concentré sur trois points:

- La compréhension du fonctionnement d'un microscope, à travers l'utilisation d'outils mathématiques et la manipulation de systèmes de lentilles simples

- La construction du cache pour le fonctionnement du microscope en champ noir, ainsi que la discussion des conditions de fonctionnement d'un tel microscope, qui s'est conclue par une mise en pratique qui a donnée les résultats donnés plus hauts. 

- L'agencement des différentes pièces du microscope afin d'avoir un moyen facile et confortable de regarder dans l'oculaire et prendre des photos avec un smartphone de ce que renvoie l'oculaire.

La première partie étant une partie théorique, elle ne possède pas d'améliorations à proprement parlé, c'est un modèle simple qui fait des hypothèses restrictives, il est possible d'utiliser des modèles plus complets, comme les transformées de Fourier, mais également plus lourds.

### Améliorations du champ noir 

En premier lieu, comme expliqué dans la partie du microscope à champ noir, l'éclairage de la pièce constitue un léger bruit de fond, l'idéal est de plonger la pièce dans le noir le plus complet. Etant donné que ce n'est pas très pratique, il serait préférable de créer une enveloppe pour le microscope, pour que la lumière externe n'intervienne pas dans le système. 

Une deuxième piste est le redimensionnement du cache, les tiges qui lui permettent de se fixer à son support sont très épaisses pour assurer une certaine solidité mais elles ont été probablement surdimensionnée. Il a été aussi expliqué plus haut que filtrer au maximum les faisceaux non parallèles est important pour un bon rendu. Il serait donc intéressant de voir si augmenter l'épaisseur du cache permet une augmentation de la qualité.

Enfin, une dernière idée qui n'est pas une amélioration en tant que tel est faisable: pour rappel, le microscope en champ noir est une technique où l'on vient filtrer dans le domaine de Fourier, ici en l'occurence, on vient retirer les vecteurs d'onde de faibles composantes $k_{x}$ et $k_{y}$. Des techniques allant dans le même sens, comme la microscopie "Schlieren", ont de fortes chances d'être construisibles.

### Améliorations du microscope smartphone

Tout d'abord, le smartphone a besoin d'une tour de soutien en plus de l'oculaire pour ne pas risquer de tomber depuis le microscope, il devrait être possible de trouver une alternative à cette tour.

Ensuite, les différents cubes du microscope sont "collés" ensemble par des joints contenant des aimants. Ces joints sont très pratiques pour aligner correctement tous les cubes. Cependant, il n'est possible d'avoir des joints à la verticale et des joints à l'horizontale sans que ceux-ci ne soient pas liés. Il serait intéressant de créer un "coude" qui permet de relier physiquement des joints verticaux et des joints horizontaux afin d'avoir un système globalement plus rigide.

Enfin, certaines pièces du microscope ont besoin d'être à des distances très spécifiques les unes des autres, comme par exemple dans le champ noir, la première lentille et la lame de verre doivent être séparées exactement de la distance focal de la lentille. L'idéal serait de créer un mécanisme permettant de déplacer l'élément précisement à l'intérieur de son cube.


